package com.example.AddSearch;

public class Car {
	private String marka;
	private double cena;
	private int przebieg;
	private int rocznik;
	private String region;
	private String miasto;
	private boolean dmg;
	private boolean neew;

	public String getMarka() {
		return marka;
	}

	public double getCena() {
		return cena;
	}

	public int getPrzebieg() {
		return przebieg;
	}

	public int getRocznik() {
		return rocznik;
	}

	public String getRegion() {
		return region;
	}

	public String getMiasto() {
		return miasto;
	}

	public boolean getDmg() {
		return dmg;
	}

	public boolean getNeew() {
		return neew;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public void setPrzebieg(int przebieg) {
		this.przebieg = przebieg;
	}

	public void setRocznik(int rocznik) {
		this.rocznik = rocznik;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public void setDmg(boolean dmg) {
		this.dmg = dmg;
	}

	public void setNeew(boolean neew) {
		this.neew = neew;
	}

}
