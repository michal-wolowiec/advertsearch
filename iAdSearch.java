package com.example.AddSearch;

public interface iAdSearch {
	
	public void sort_cena(double[] cena);
	
	public void sort_rok(int[] rok);
	
	public void sort_adDate(String[] adDate);
	
	public void sort_przebeg(int[] przebieg);

}
