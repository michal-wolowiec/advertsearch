package com.example.AddSearch;

import java.util.Arrays;

public class AdSearch implements iAdSearch {
	
	private String dataDodania;

	@Override
	public void sort_cena(double[] cena) {
		Arrays.sort(cena);
		
	}

	@Override
	public void sort_rok(int[] rok) {
		Arrays.sort(rok);
		
	}

	@Override
	public void sort_adDate(String[] adDate) {
		Arrays.sort(adDate);
		
	}

	@Override
	public void sort_przebeg(int[] przebieg) {
		Arrays.sort(przebieg);
		
	}

	public String getDataDodania() {
		return dataDodania;
	}

	public void setDataDodania(String dataDodania) {
		this.dataDodania = dataDodania;
	}
	

}
